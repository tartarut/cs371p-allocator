    // ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <sstream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const my_allocator&, const my_allocator&) {
            return false;}                                                   // this is correct

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
            return !(lhs == rhs);}

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * <your documentation>
         */
        bool valid () const {
            // <your code>
            int index = 0;
            bool free = false;
            
            while(index < N) {
                //printHeap();
                int size1 = (*this)[index];
                //cout << "valid size2 " << index + (size1 < 0 ? -size1 : size1) - 4 << endl;
                int size2 = (*this)[index + (size1 < 0 ? -size1 : size1) - 4];
                //printHeap();
                if(size1 != size2) {
                    cout << "INVALID " << "size1 != size2 " << size1 << " != " << size2 << endl;
                    return false;
                }
                if(size1 < 0) {
                    free = false;
                    index -= size1;
                }
                else {
                    if(free == true){
                        cout << "INVALID " << "free == true" << endl;
                        return false;
                    }
                    free = true;
                    
                    index += size1;
                }
                
            }
            
            if(index != N) {
                cout << "INVALID " << "index != N " << index << endl;
                return false;
            }
            return true;}

    public:
        // ------------
        // constructors
        // ------------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            if(N < sizeof(T) + (2 * sizeof(int))) {
                std::bad_alloc exception;
                throw exception;
            }
            (*this)[0] = N; // replace!
            // <your code>
            (*this)[N-4] = N;
            //cout << "constructor N=" << N << " sizeof(T)=" << sizeof(T) << endl;
            //printHeap();
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
            pointer allocate (size_type s) {
                // <your code>
                int bytes = s * sizeof(T) + (2 * sizeof(int));
                int index = 0;
                while(index < N) {
                    int size = (*this)[index];
                    if(bytes <= size) {
                        //free block of required size
                        if(size - bytes < sizeof(T) + 8) {
                            //gives the whole free block
                            (*this)[index + size - 4] = -size;
                            (*this)[index] = -size;
                            return (pointer)&a[index + 4];
                        }
                        else {
                            //splits freeblock
                            (*this)[index] = -bytes;
                            (*this)[index + bytes - 4] = -bytes;
                            (*this)[index + bytes] = size - bytes;
                            (*this)[index + size - 4] = size - bytes;
                            return (pointer)&a[index + 4];
                        }
                    }
                    if(size < 0) {
                        index -= size;
                    }
                    else {
                        index += size;
                    }
                }

                assert(valid());
                return nullptr;}           // replace!

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         */
        void deallocate (pointer p, size_type s) {
            // <your code>
            //int index = ((char*)p) - a - 4;
            //finds the nth busy bock
            int index = 0;
            int counter = 0;
            while(index < N) {
                int size = (*this)[index];
                if(size < 0) {
                    ++counter;
                    if(counter == s){
                        break;
                    }
                    index -= size;
                }
                else {
                    index += size;
                }
            }
            //if nth busy block is not found
            if (counter != s)
                return;
            
            //cout << "index " << index << endl;
            int negSize = (*this)[index];
            //cout << "negSize " << negSize << endl;
            //if invalid pointer
            if(negSize > 0) {
                throw std::invalid_argument("");
                //throw exception
            }
            int size = -negSize;
            //cout << "size " << size << endl;
            if(size != (*this)[index + size - 4]) {
                //throw exception
            }
            //printHeap();
            
            //freeing block
            (*this)[index] = size;
            //cout << "a[index] " << (*this)[index] << endl;
            //printHeap();
            (*this)[index + size - 4] = size;
            
            //merges left block if free
            if(index != 0 && (*this)[index - 4] > 0) {
                int leftSize = (*this)[index - 4];
                //cout << "leftSize " << leftSize << endl;
                (*this)[index - leftSize] = leftSize + size;
                (*this)[index + size - 4] = leftSize + size;
                index = index - leftSize;
                size = leftSize + size;
            }
            
            //merges right block if free
            if(index + size != N && (*this)[index + size] > 0) {
                int rightSize = (*this)[index + size];
                //cout << "rightSize " << rightSize << endl;
                //printHeap();
                (*this)[index] = rightSize + size;
                (*this)[index + size + rightSize - 4] = rightSize + size;
                //printHeap();
            }
            
            //cout << "index " << index << endl;
            
            //printHeap();
            assert(valid());}

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        void printHeap() const {
            int index = 0;
            while(index < N) {
                int size = (*this)[index];
                if(size < 0) {
                    index -= size;
                    size += 8;
                }
                else {
                    index += size;
                    size -= 8;
                }
                cout << size << " ";
            }
            cout << endl;
        }};

#endif // Allocator_h
