// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <sstream>
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <string>   // s

#include "Allocator.h"

// ----
// main
// ----

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    string s;
    getline(cin, s);
    istringstream sin(s);
    int T;
    sin >> T;
    getline(cin, s);
    for(int t = 0; t < T; t++) {
        //cout << t + 1 << endl;
        vector<double*> pointers; 
        my_allocator<double, 1000> mA;
        int A;
        while(getline(cin, s)) {
            if(s.empty()) {
                mA.printHeap();
                break;
            }
            istringstream sin(s);
            sin >> A;
            
            if(A < 0) {
                if(-A - 1 < pointers.size()) {
                    //cout << "deallocate " << *pointers[-A - 1] << endl;
                    mA.deallocate(pointers[-A - 1], -A);
                    pointers.erase(begin(pointers) - A - 1);
                } 
            }
            else {
                double* pointer = mA.allocate(A);
                *pointer = (double) A;
                pointers.push_back(pointer);
                //cout << "allocate " << *pointer << endl;
            }
        }
        if(cin.eof())
            mA.printHeap();
    }
    return 0;
}