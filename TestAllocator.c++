// -----------------------------------------
// projects/c++/allocator/TestAllocator1.c++
// Copyright (C) 2017
// Glenn P. Downing
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            std::allocator<int>,
            std::allocator<double>,
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);}}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

// --------------
// TestAllocator2
// --------------

TEST(TestAllocator2, const_index) {
    const my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 100);}

TEST(TestAllocator2, index) {
    my_allocator<int, 100> x;
    ASSERT_EQ(x[0], 100);}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);}}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 10;
    const value_type     v = 2;
    const pointer        b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}



template <typename A>
struct TestAllocator4 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_4;

TYPED_TEST_CASE(TestAllocator4, my_types_4);

TYPED_TEST(TestAllocator4, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 3;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
        x.deallocate(p, 1);}}

template <typename A>
struct TestAllocator5 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    8>,
            my_allocator<double, 8>>
        my_types_5;

TYPED_TEST_CASE(TestAllocator5, my_types_5);

TYPED_TEST(TestAllocator5, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    
    try {
        allocator_type x;
        ASSERT_EQ(1, 2);
    }
    catch (...) {
        ASSERT_EQ(1, 1);
    }}

    template <typename A>
struct TestAllocator6 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_6;

TYPED_TEST_CASE(TestAllocator6, my_types_6);

TYPED_TEST(TestAllocator6, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 2;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        x.destroy(p);
        x.destroy(p+1);}}


    template <typename A>
        struct TestAllocator7 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_7;

TYPED_TEST_CASE(TestAllocator7, my_types_7);

TYPED_TEST(TestAllocator7, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 4;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
                x.construct(p+3, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
                ASSERT_EQ(v, *(p+3));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
                x.destroy(p+3);
        x.deallocate(p, 1);}}








    template <typename A>
        struct TestAllocator8 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_8;

        TYPED_TEST_CASE(TestAllocator8, my_types_8);

TYPED_TEST(TestAllocator8, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 4;
    const value_type     v = 20;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
                x.construct(p+3, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
                ASSERT_EQ(v, *(p+3));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
                x.destroy(p+3);
        x.deallocate(p, 1);}}

template <typename A>
struct TestAllocator9 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_9;

TYPED_TEST_CASE(TestAllocator9, my_types_9);

TYPED_TEST(TestAllocator9, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 2;
    const value_type     v = 6;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        x.destroy(p);
        x.destroy(p+1);}}

            template <typename A>
        struct TestAllocator11 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_11;

        TYPED_TEST_CASE(TestAllocator11, my_types_11);

TYPED_TEST(TestAllocator11, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 5;
    const value_type     v = 7;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
                x.construct(p+3, v);
                x.construct(p+4, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
                ASSERT_EQ(v, *(p+3));
                  ASSERT_EQ(v, *(p+4));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
                x.destroy(p+3);
                   x.destroy(p+4);
        x.deallocate(p, 1);}}

                    template <typename A>
        struct TestAllocator12 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_12;

        TYPED_TEST_CASE(TestAllocator12, my_types_12);

TYPED_TEST(TestAllocator12, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 5;
    const value_type     v = 34;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
                x.construct(p+3, v);
                x.construct(p+4, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
                ASSERT_EQ(v, *(p+3));
                  ASSERT_EQ(v, *(p+4));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
                x.destroy(p+3);
                   x.destroy(p+4);
        x.deallocate(p, 1);}}

    template <typename A>
        struct TestAllocator14 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_14;

TYPED_TEST_CASE(TestAllocator14, my_types_14);

TYPED_TEST(TestAllocator14, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 4;
    const value_type     v = 16;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        x.construct(p+1, v);
        x.construct(p+2, v);
                x.construct(p+3, v);
        ASSERT_EQ(v, *p);
        ASSERT_EQ(v, *(p+1));
        ASSERT_EQ(v, *(p+2));
                ASSERT_EQ(v, *(p+3));
        x.destroy(p);
        x.destroy(p+1);
        x.destroy(p+2);
                x.destroy(p+3);
        x.deallocate(p, 1);}}


    template <typename A>
        struct TestAllocator15 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;};

typedef testing::Types<
            my_allocator<int,    100>,
            my_allocator<double, 100>>
        my_types_15;

TYPED_TEST_CASE(TestAllocator15, my_types_15);

TYPED_TEST(TestAllocator15, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

          allocator_type x;
    const size_type      s = 4;
    const value_type     v = 106;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, 1);}}